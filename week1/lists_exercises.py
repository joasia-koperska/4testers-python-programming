if __name__ == '__main__':
    favourite_movies = ["Dune", "Stalker", "Lost Highway", "Clockwork Orange", "Mad Max"]

    movies_length = len(favourite_movies)
    print(f"The number of my favourite movies is {movies_length}")

    first_movie = favourite_movies[0]
    last_movie = favourite_movies[movies_length -1]
    print(f"First movie: {first_movie}, last movie: {last_movie}")

    # Add element at the end of the list
    favourite_movies.append("Star Wars")
    print(f"Last movie now: {favourite_movies[-1]}")

    # Replace element at index 1
    favourite_movies[1] = "Solaris"
    print(f"Favorites movies now: {favourite_movies}")

    favourite_movies.insert(1, "John Wick")