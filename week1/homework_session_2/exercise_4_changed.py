def average_students_grades(grades_list):
    not_rounded_average = sum(grades_list)  / len(grades_list)
    rounded_average = round(not_rounded_average, 2)
    return rounded_average


if __name__ == '__main__':
    student_grades_a = [5, 3, 2, 4, 1, 1, 3, 5, 4]
    print(f"Student a grade point average is:", average_students_grades(student_grades_a))
