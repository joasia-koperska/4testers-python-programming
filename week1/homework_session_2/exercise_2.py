def generate_email(firstname, lastname):
    return f"{firstname}.{lastname}@4testers.pl".lower()


if __name__ == '__main__':
    print(generate_email("Janusz", "Nowak"))
    print(generate_email("Barbara", "Kowalska"))