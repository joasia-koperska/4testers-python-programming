if __name__ == '__main__':
    emails = ['a@example.com', 'b@example.com']
    print(f"List length: {len(emails)}")
    print(f"First element: {emails[0]}")
    print(f"Last element: {emails[len(emails)-1]}")

    # add element to the list
    emails.append('cde@example.com')
    print(f"Last element: {emails[len(emails)-1]}")
    print(emails)