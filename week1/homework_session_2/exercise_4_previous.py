def get_middle_element(list):
    length = len(list)
    print(f"Length: {length}")
    middle = round((length-1)/2)
    print(f"Middle: {middle}")
    return list[middle]


if __name__ == '__main__':
    list = ["Ania", "Ola", "Kasia", "Tomek", "Adam", "Krzysiu"]
    print("Middle element 1:", get_middle_element(list))