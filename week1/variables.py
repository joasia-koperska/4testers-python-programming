friends_name = "Tomasz"
friends_age = 37
friends_number_of_pets = 2
friend_has_driving_licence = True
friendship_length_in_years = 13.5

print("My friend's name:", friends_name)
print("My friend's age:", friends_age)
print("My friend's number of pets:", friends_number_of_pets)
print("My friend has driving licence:", friend_has_driving_licence)
print("Friendship length:", friendship_length_in_years)
