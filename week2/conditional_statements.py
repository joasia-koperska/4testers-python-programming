def print_whether_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    elif temperature_in_celsius > 0:
        print("It's quite OK")
    else:
        print("It's getting cold :(")

def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False
    # return True if age >= 18 else False


def check_if_standard_conditions(temp_in_celsius, pressure_in_hpa):
    if temp_in_celsius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False


def get_grade_for_test_percentage(test_percentage):
    if test_percentage >= 90:
        return 5
    elif test_percentage >=75:
        return 4
    elif test_percentage >=50:
        return 3
    else:
        return 2


if __name__ == '__main__':

    temperature_in_celsius = 26
    print_whether_temperature_description(temperature_in_celsius)


    age_kate = 17
    age_tom = 18
    age_marta = 21
    print("Kate", is_person_an_adult(age_kate))
    print("Tom", is_person_an_adult(age_tom))
    print("Marta", is_person_an_adult(age_marta))


    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa {check_if_standard_conditions(0, 1013)}")
    print(f"Checking if standard conditions for 0 Celsius, 1014 hPa {check_if_standard_conditions(0, 1014)}")
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa {check_if_standard_conditions(1, 1013)}")
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa {check_if_standard_conditions(1, 1014)}")


    print(f"Grade for 99% is {get_grade_for_test_percentage(99)}")
    print(f"Grade for 90% is {get_grade_for_test_percentage(90)}")
    print(f"Grade for 89% is {get_grade_for_test_percentage(89)}")
    print(f"Grade for 50% is {get_grade_for_test_percentage(50)}")
    print(f"Grade for 49% is {get_grade_for_test_percentage(49)}")